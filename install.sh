#!/bin/sh

#Apps a Instalar
sudo apt-get install gedit -y
sudo apt-get install golang -y
sudo apt-get install apktool -y
sudo apt-get install terminator -y
sudo apt-get install tor -y
sudo apt-get install proxychains -y
sudo apt-get install leafpad -y
sudo apt-get install steghide -y
sudo apt-get install fcrackzip -y
sudo apt-get install pdfcrack -y
sudo apt-get install wfuzz -y
sudo apt-get install openvpn -y
sudo apt-get install prips -y
sudo apt-get install exiftool -y
sudo apt-get install gnome-terminal -y
sudo apt-get install caja -y
sudo apt-get install ruby-full -y
sudo apt-get install fish -y
sudo apt-get install sonic-visualiser -y
sudo apt-get install gobuster -y

pip install paramiko==2.0.8
gem install zsteg
gem install evil-winrm
pip3 install stegoveritas
stegoveritas_install_deps

#Instalacion de Docker
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
echo 'deb https://download.docker.com/linux/debian stretch stable' > /etc/apt/sources.list.d/docker.list
apt-get update
apt-get remove docker docker-engine docker.io
apt-get install docker-ce -y
service docker start

docker pull opensecurity/mobile-security-framework-mobsf

#Binario go
go get -u github.com/tomnomnom/unfurl
go get -u github.com/tomnomnom/assetfinder
go get -u github.com/tomnomnom/httprobe
go get github.com/tomnomnom/waybackurls

mkdir -v /opt/git
mkdir -v /opt/ngrok

cd sources
git clone https://gitlab.com/Cyb3r3xpl0it/whichos.git
cd whichos
mv whichos.py os.py
chmod +x os.py
mv os.py /usr/local/bin
cd ..

wget https://github.com/projectdiscovery/subfinder/releases/download/v2.4.8/subfinder_2.4.8_linux_amd64.tar.gz
tar -xvf subfinder_2.4.8_linux_amd64.tar.gz
mv subfinder /usr/local/bin

wget https://github.com/michenriksen/aquatone/releases/download/v1.7.0/aquatone_linux_amd64_1.7.0.zip
unzip aquatone_linux_amd64_1.7.0.zip
mv aquatone /usr/local/bin

wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
mv ngrok-stable-linux-amd64.zip /opt/ngrok
unzip /opt/ngrok/ngrok-stable-linux-amd64.zip
rm /opt/ngrok/ngrok-stable-linux-amd64.zip

wget https://github.com/angryip/ipscan/releases/download/3.7.6/ipscan_3.7.6_amd64.deb
dpkg -i ipscan_3.7.6_amd64.deb

wget https://github.com/sharkdp/bat/releases/download/v0.18.1/bat_0.18.1_amd64.deb
dpkg -i bat_0.18.1_amd64.deb

wget https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd_0.20.1_amd64.deb
dpkg -i lsd_0.20.1_amd64.deb

wget https://github.com/sharkdp/fd/releases/download/v8.2.1/fd_8.2.1_amd64.deb
dpkg -i fd_8.2.1_amd64.deb

wget https://www.tenable.com/downloads/api/v1/public/pages/nessus/downloads/13035/download?i_agree_to_tenable_license_agreement=true
mv download\?i_agree_to_tenable_license_agreement=true Nessus-8.15.0-debian6-amd64.deb
dpkg -i Nessus-8.15.0-debian6-amd64.deb

git clone https://gitlab.com/Cyb3r3xpl0it/fasttcpscan.git
go build -ldflags "-s -w" fastTCPScan.go
upx fastTCPScan
mv fastTCPScan /usr/local/bin

git clone https://github.com/Und3rf10w/kali-anonsurf.git

#Recon

mkdir -v /opt/git/recon

git clone -v -4 https://github.com/nahamsec/JSParser.git /opt/git/recon/JSParser
git clone -v -4 https://github.com/maurosoria/dirsearch.git /opt/git/recon/dirsearch
git clone -v -4 https://github.com/nahamsec/lazys3.git /opt/git/recon/lazys3
git clone -v -4 https://github.com/tomdev/teh_s3_bucketeers.git /opt/git/recon/teh_s3_bucketeers
git clone -v -4 https://github.com/jobertabma/virtual-host-discovery.git /opt/git/recon/virtual-host-discovery
git clone -v -4 https://github.com/nahamsec/lazyrecon.git /opt/git/recon/lazyrecon
git clone -v -4 https://github.com/blechschmidt/massdns.git /opt/git/recon/massdns
git clone -v -4 https://github.com/yassineaboukir/asnlookup.git /opt/git/recon/asnlookup
git clone -v -4 https://github.com/nahamsec/crtndstry.git /opt/git/recon/crtndstry
git clone -v -4 https://github.com/nahamsec/bbht.git /opt/git/recon/bbht
git clone -v -4 https://github.com/guelfoweb/knock.git /opt/git/recon/knock-dns
git clone -v -4 https://github.com/carlospolop/legion.git /opt/git/recon/legion
git clone -v -4 https://github.com/SecHackLabs/webhackshl.git /opt/git/recon/webhackshl
git clone -v -4 https://github.com/aboul3la/Sublist3r.git /opt/git/recon/Subist3r
git clone -v -4 https://github.com/antichown/subdomain-takeover.git /opt/git/recon/subdomain-takeover
git clone -v -4 https://github.com/sherlock-project/sherlock.git /opt/git/recon/sherlock
git clone -v -4 https://gitlab.com/Cyb3r3xpl0it/userrecon.git /opt/git/recon/userrecon

#Exploting

mkdir -v /opt/git/exploting

git clone -v -4 https://github.com/codingo/NoSQLMap.git /opt/git/exploting/NoSQLMap
git clone -v -4 https://github.com/iphelix/pack.git /opt/git/exploting/Cracking-Pack-Toolkit
git clone -v -4 https://github.com/Rhynorater/CVE-2018-15473-Exploit.git /opt/git/exploting/CVE-2018-15473-Exploit
git clone -v -4 https://github.com/elceef/dnstwist.git /opt/git/exploting/dnstwist
git clone -v -4 https://github.com/M4sc3r4n0/Evil-Droid.git /opt/git/exploting/Evil-Droid
git clone -v -4 https://github.com/s0md3v/Hash-Buster.git /opt/git/exploting/Hash-Buster
git clone -v -4 https://github.com/hashcat/hashcat-utils.git /opt/git/exploting/Hashcat-Utils
git clone -v -4 https://github.com/magnumripper/JohnTheRipper.git /opt/git/exploting/JohnTheRipper
git clone -v -4 https://github.com/AlessandroZ/LaZagne.git /opt/git/exploting/laZagne
git clone -v -4 https://github.com/AlessandroZ/LaZagneForensic.git /opt/git/exploting/laZagneForensic
git clone -v -4 https://github.com/ZettaHack/PasteZort.git /opt/git/exploting/PasteZort
git clone -v -4 https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git /opt/git/exploting/privilege-escalation-awesome-scripts-suite
git clone -v -4 https://github.com/Screetsec/TheFatRat.git /opt/git/exploting/TheFatRat
git clone -v -4 https://github.com/HackingEnVivo/WaterDoS.git /opt/git/exploting/WaterDoS
git clone -v -4 https://github.com/WhiteWinterWolf/wwwolf-php-webshell.git /opt/git/exploting/wwwolf-php-webshell
git clone -v -4 https://github.com/GinjaChris/pentmenu.git /opt/git/exploting/pentmenu
git clone -v -4 https://github.com/PowerShellMafia/PowerSploit /opt/git/exploting/PowerSploit

#phising
mkdir -v /opt/git/phishing

git clone -v -4 https://github.com/samyoyo/weeman.git /opt/git/phishing/weeman
git clone -v -4 https://github.com/gophish/gophish.git /opt/git/phishing/gophish
git clone -v -4 https://github.com/trustedsec/social-engineer-toolkit.git /opt/git/phishing/setoolkit

#Wordlist

mkdir /opt/git/wordlists

git clone -v -4 https://github.com/danielmiessler/SecLists.git /opt/git/wordlists/SecLists
git clone -v -4 https://github.com/swisskyrepo/PayloadsAllTheThings.git /opt/git/wordlists/PayloadsAllTheThings
git clone -v -4 https://github.com/fuzzdb-project/fuzzdb.git /opt/git/wordlists/fuzzdb
git clone -v -4 https://github.com/duyetdev/bruteforce-database.git /opt/git/wordlists/bruteforce-database
git clone -v -4 https://github.com/berzerk0/Probable-Wordlists.git /opt/git/wordlists/Probable-Wordlists

echo 'mv /usr/local/bin'
echo 'Foxy Proxy'
echo 'Privacy Badger'
echo 'Wappalyzer'
echo 'User Agent Manager'
echo 'COokies Manager'

echo "deb http://http.kali.org/kali kali-last-snapshot main non-free contrib" >> /etc/apt/sources.list
